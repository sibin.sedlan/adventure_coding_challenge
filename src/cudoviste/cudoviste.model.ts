import { Oruzije } from '../oruzije/oruzije.interface';
import { Udarac } from '../oruzije/udarac.model';

export abstract class Cudoviste {
    health = 80;
    naziv= 'Cudoviste';
    udarac = new Udarac();

    napada(): Oruzije {
        return this.udarac;
    }
}