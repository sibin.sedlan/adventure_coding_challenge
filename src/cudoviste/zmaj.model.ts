import { Npc } from '../npc';
import { randomize } from '../utils';
import { Cudoviste } from './cudoviste.model';
import { BljuvanjeVatre } from '../oruzije/bluvanje-vatre.model';
import { Oruzije } from '../oruzije/oruzije.interface';

export class Zmaj extends Cudoviste implements Npc {
    naziv = 'Zmaj'
    health = 100;
    bljuvanjeVatre = new BljuvanjeVatre()

    napada(): Oruzije {
        if (randomize()) {
            return this.bljuvanjeVatre;
        } else {
            return super.napada();
        }
    }
}