import { Cudoviste } from './cudoviste.model';
import { randomize } from '../utils';
import { Npc } from '../npc';
import { Oruzije } from '../oruzije/oruzije.interface';
import { Ujed } from '../oruzije/ujed.model';

export class Pauk extends Cudoviste implements Npc {
    naziv = 'Pauk';
    health = 80;
    ujed = new Ujed()

    napada(): Oruzije {
        if (randomize()) {
            return this.ujed;
        } else {
            return super.napada();
        }
    }
}