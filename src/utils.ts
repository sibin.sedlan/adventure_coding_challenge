export const randomize = (): boolean => Math.floor(Math.random() * 100) > 50;

export const posledicaNapada = (napadac: string, zrtva: string, nazivNapada: string): string => `${napadac} je napao ${zrtva} pomocu ${nazivNapada}`;