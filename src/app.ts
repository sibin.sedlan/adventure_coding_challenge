import { Zmaj } from './cudoviste/zmaj.model';
import { Macevalac } from './heroj/macevalac.model';
import { Koplje } from './oruzije/koplje.model';
import { Mac } from './oruzije/mac.model';
import { randomize, posledicaNapada } from './utils';
import { Carobnjak } from './heroj/carobnjak.model';
import { Carolija } from './oruzije/carolija.model';
import * as fs from 'fs';
import { Heroj } from './heroj/heroj.model';
import { Cudoviste } from './cudoviste/cudoviste.model';

const simulirajBitku = (heroj: Heroj, cudoviste: Cudoviste): string => {
    let bitka = 'Neka bitka pocne!';

    while (cudoviste.health > 0 && heroj.health > 0) {
        if (randomize()) {
            const napad = cudoviste.napada();
            heroj.health -= napad.steta;
            bitka += '\n' + posledicaNapada(cudoviste.naziv, heroj.naziv, napad.naziv)
        } else {
            const napad = heroj.napada();
            cudoviste.health -= napad.steta;
            bitka += '\n' + posledicaNapada(heroj.naziv, cudoviste.naziv, napad.naziv)
        }
    }

    if (cudoviste.health <= 0) {
        bitka += '\n' + `${heroj.naziv} je pobedio u duelu sa ${cudoviste.naziv}!`;
    }

    if (heroj.health <= 0) {
        bitka += '\n' + `${cudoviste.naziv} je pobedio u duelu sa ${heroj.naziv}`;
    }

    bitka += '\nBitka je zavrsena!'

    return bitka;
}

const simulirajOruzije = (heroj1: Heroj, heroj2: Heroj): string => {
    let log = '';

    const oruzija = [new Mac()];

    try {
        heroj1.pokupiOruzije(oruzija[0]);
        log += `${heroj1.naziv} je pokupio oruzije ${oruzija[0].naziv}\n`;
        oruzija.shift();
    } catch (error) {
        log += `${error.message}\n`;
    }
    try {
        const bacenoOruzije = heroj1.baciOruzije();
        oruzija.push(bacenoOruzije);
        log += `${heroj1.naziv} je bacio oruzije ${bacenoOruzije.naziv}\n`;
    } catch (error) {
        log += `${error.message}\n`;
    }
    try {
        heroj2.pokupiOruzije(oruzija[0]);
        oruzija.shift();
        log += `${heroj2.naziv} je pokupio oruzije ${oruzija[0].naziv}\n`;
    } catch (error) {
        log += `${error.message}\n`;
    }

    return log;
}

const rezultatBitke = simulirajBitku(new Macevalac(new Mac(), [new Koplje(), new Mac()]), new Zmaj());
const kupljenjeOruzija = simulirajOruzije(new Macevalac(new Koplje(), [new Koplje()]), new Carobnjak(new Carolija(), [new Carolija()]));

fs.writeFile('log.txt', rezultatBitke + '\n' + kupljenjeOruzija, (err) => {
    if (err) throw err;

    console.log('Done!');
});