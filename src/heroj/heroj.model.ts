import { Oruzije } from '../oruzije/oruzije.interface';

export abstract class Heroj {
    naziv = 'Heroj';
    health = 100;
    aktivnoOruzije: Oruzije
    oruzijeURancu: Array<Oruzije>;
    constructor(aktivnoOruzije: Oruzije, oruzijeURancu: Array<Oruzije>) {
        this.aktivnoOruzije = aktivnoOruzije;
        this.oruzijeURancu = oruzijeURancu;
    }

    napada(): Oruzije {
        return this.aktivnoOruzije;
    }

    baciOruzije(): Oruzije {
        if (this.oruzijeURancu.length === 0) {
            throw new Error('NoWeaponException');
        } else {
            const bacenoOruzije = this.aktivnoOruzije;
            this.aktivnoOruzije = this.oruzijeURancu[0];
            return bacenoOruzije;
        }
    }

    zameniOruzije(): void {
        if (this.oruzijeURancu.length === 0) {
            throw new Error('NoWeaponException');
        } else {
            this.oruzijeURancu.push(this.aktivnoOruzije);
            this.aktivnoOruzije = this.oruzijeURancu[0];
            this.oruzijeURancu.shift();
        }
    }

    pokupiOruzije(oruzije: Oruzije): void {
        if (!this.aktivnoOruzije) {
            this.aktivnoOruzije = oruzije;
        } else {
            if (this.oruzijeURancu.length > 2) {
                throw new Error('ExceededMaxumCarryingCapactiyException');
            } else {
                this.oruzijeURancu.push(oruzije);
            }
        }
    }
}