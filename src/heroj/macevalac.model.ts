import { Heroj } from "./heroj.model";
import { Oruzije } from '../oruzije/oruzije.interface';
import { Npc } from "../npc";

export class Macevalac extends Heroj implements Npc {
    naziv = 'Macevalac';

    pokupiOruzije(oruzije: Oruzije): void {
        if (oruzije.steta === 20) {
            throw new Error('IncompatibleWeapon exception');
        } else {
            super.pokupiOruzije(oruzije);
        }
    }
}