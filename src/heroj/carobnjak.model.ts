import { Npc } from '../npc';
import { Oruzije } from '../oruzije/oruzije.interface';
import { Heroj } from './heroj.model';

export class Carobnjak extends Heroj implements Npc {
    naziv = 'Carobnjak';
    health = 150;

    pokupiOruzije(oruzije: Oruzije): void {
        if (oruzije.steta !== 20) {
            throw new Error('IncompatibleWeaponException');
        } else {
            super.pokupiOruzije(oruzije);
        }
    }
}